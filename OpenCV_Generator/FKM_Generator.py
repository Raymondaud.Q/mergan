# Quentin RAYMONDAUD
# Fake merge generator
# Free to use

import cv2
import IMG
import numpy as np

emojisPath = "../DataSet/emojis/"
images, filenames = IMG.load_images_from_folder( emojisPath, 0, 500, True)
index = 0
for iD in range(len(images)-1) :
	i = images[iD]
	i2 = images[iD+1]

	if ( i2.shape == i.shape ): # EN ATTENDANT DE RAJOUTER LE CANAL ALPHA SUR LES IMAGES QUI N'EN ONT PAS

		## A AMELIORER ! ##
		yValue=IMG.find_horizontal_limit(cv2.cvtColor(i, cv2.COLOR_RGBA2GRAY),cv2.cvtColor(i2,cv2.COLOR_RGBA2GRAY))
		## A AMELIORER ! ##

		top, bottom = IMG.horizontal_crop(i,yValue=yValue)
		top2, bottom2 = IMG.horizontal_crop(i2,yValue=yValue)

		## PREMIERE COMBINAISON
		merge1 = np.zeros(i.shape, np.uint8)
		merge1 = IMG.merge_img_horizontal(merge1, top, 0, yValue)
		merge1 = IMG.merge_img_horizontal(merge1, bottom2, yValue, i.shape[0])
		cv2.imshow("merge1", merge1)
		cv2.waitKey(100)
		res = input('Save ? [y, else = no ]')
		if ( res == 'y' ):
			cv2.imwrite("../DataSet/FKM/generated"+str(index)+".png", merge1)
			index = index + 1

		## DEUXIEME COMBINAISON
		merge1 = np.zeros(i.shape, np.uint8)
		merge1 = IMG.merge_img_horizontal(merge1, top2, 0, yValue)
		merge1 = IMG.merge_img_horizontal(merge1, bottom, yValue, i.shape[0])
		cv2.imshow("merge1", merge1)
		cv2.waitKey(100)
		res = input('Save ? [y, else = no ]')
		if ( res == 'y' ):
			cv2.imwrite("../DataSet/FKM/generated"+str(index)+".png", merge1)
			index = index + 1

# import random

#cv2.imshow("top", cv2.cvtColor(top, cv2.COLOR_RGBA2GRAY))
#cv2.imshow("bottom",bottom)
#cv2.imshow("top2", cv2.cvtColor(top2, cv2.COLOR_RGBA2GRAY))
#cv2.imshow("bottom2",bottom2)
#cv2.waitKey()

## Creates FAKE merge images into /PNG_FAKE_96_32/ based on real /PNG_EMOJI_96_32/ images
## fullImgPath = "../DataSet/EMOJI/PNG_EMOJI_96_32/"
## fakeFullImgPath = "../DataSet/EMOJI/PNG_FAKE_96_32/"
## images, filenames = IMG.load_images_from_folder( fullImgPath, 0, 21, True)
## 
## index=0
## for i in images :
## 	fakeI = IMG.merge_img( IMG.vertical_crop(i,random.randint(0, 1)), i, 2)
## 	cv2.imwrite( fakeFullImgPath + filenames[index],fakeI )
## 	index+=1


## test = cv2.imread("../DataSet/emojis/56-hot-face-joy.png",cv2.IMREAD_UNCHANGED)
## test2 = cv2.imread("../DataSet/emojis/1-grinning-face-apple.png",cv2.IMREAD_UNCHANGED)
## 
## top, bottom = IMG.lateral_crop(test)
## top, bottom = IMG.horizontal_crop(test2)
## cv2.imwrite("output2.png", bottom)
## cv2.imwrite("output.png",top)
## 
## grayImage = cv2.cvtColor(test2, cv2.COLOR_RGBA2GRAY)
## top, bottom = IMG.horizontal_crop(test2, yValue=IMG.find_horizontal_limit(grayImage))
## cv2.imwrite("automatedTOP.png", top)
## cv2.imwrite("automatedBOTTOM.png", bottom)

